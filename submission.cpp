/// Enter your code here
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/photo.hpp"

#include <iostream>

using namespace cv;
using namespace std;

// Declare Mat objects for original image and mask for inpainting
Mat img, inpaintMask;
// Mat object for result output
Point prevPt(-1, -1);

// onMouse function for Mouse Handling
// Used to draw regions required to inpaint
static void onMouse(int event, int x, int y, int flags, void*)
{
	Mat res;
	img.copyTo(res);
	
	if (event == EVENT_MOUSEMOVE)
		prevPt = Point(-1, -1);
	else if (event == EVENT_LBUTTONDOWN)
	{
		prevPt = Point(x, y);
		circle(res, prevPt, 15, Scalar(255, 255, 255), 1, LINE_AA);
		imshow("Blemish Removal", res);
		
	}
		
	else if (event == EVENT_LBUTTONUP)
	{
		res.copyTo(img);
		
		Point pt(x, y);
		if (prevPt.x < 0)
			prevPt = pt;
		circle(inpaintMask, prevPt, 1, Scalar(255, 255, 255), 15, LINE_AA);
		circle(img, prevPt, 1, Scalar(255, 255, 255), 15, LINE_AA);

		prevPt = pt;
		inpaint(img, inpaintMask, img, 3, INPAINT_NS);
		imshow("Blemish Removal", img);
		//imshow("image: mask", inpaintMask);
	}
}


int main(int argc, char** argv)
{

	string filename;
	if (argc > 1)
		filename = argv[1];
	else
		filename = "blemish.png";

	// Read image in color mode
	img = imread(filename, IMREAD_COLOR);
	Mat img_mask;
	// Return error if image not read properly
	if (img.empty())
	{
		cout << "Failed to load image: " << filename << endl;
		return 0;
	}

	namedWindow("Blemish Removal", WINDOW_AUTOSIZE);

	// Create a copy for the original image
	img_mask = img.clone();
	// Initialize mask (black image)
	inpaintMask = Mat::zeros(img_mask.size(), CV_8U);

	// Show the original image
	imshow("Blemish Removal", img);
	setMouseCallback("Blemish Removal", onMouse, NULL);

	waitKey(0);

	return 0;
}
